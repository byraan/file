const commandLineArgs=require('../lib/command-line-args'),
log=require('../lib/log');
//const JSDOM=require('jsdom').JSDOM;
//const pdf=require("pdf-creator-node");
const puppeteer = require('puppeteer');
const fs=require('fs'),
path=require('path');

commandLineArgs({}, [
	{
		name: 'input',
		alias: 'i',
		type: String,
		defaultValue: ['.'],
		multiple: true
	},
	{
		name: 'output',
		alias: 'o',
		type: String,
		defaultValue: '.'
	},
	{
		name: 'name',
		alias: 'n',
		type: String,
		defaultValue: 'result'
	}
], 0);

/* var pdfOptions = {
	width: '600px',
	orientation: 'portrait',
	renderDelay: 500,
	border: "0"
}; */

var buildPdfIterator = 0;
var buildPdfTotal = process.arguments.input.length;
async function buildPdf(input, iterator) {

	var type = 'file';
	if (input.substr(0, 4) == 'http') type = 'url';

	var fileName = process.arguments.name + (buildPdfTotal > 1 ? buildPdfIterator : '');
	var pdfPath = path.resolve(process.arguments.output) + '/' + fileName + '.pdf';
	buildPdfIterator++;
	if (type == 'file') {
		var htmlContent = fs.readFileSync(input, 'utf-8').toString();
		fileName = path.basename(input, '.html');
		process.arguments.output = process.arguments.output ? path.resolve(process.arguments.output) : path.dirname(input);
		pdfPath = process.arguments.output + '/' + fileName + '.pdf';
	}

	//////
	//////
	//////
	//////
	//////
	//////

	// puppeteer

	// Create a browser instance
	const browser = await puppeteer.launch();

	// Create a new page
	const page = await browser.newPage();

	await page.setViewport({
		width: 1920,
		height: 0,
		deviceScaleFactor: 1,
	});

	// Open URL in current page
	if (type == 'file') page.setContent(htmlContent, { waitUntil: 'domcontentloaded' });
	else if (type == 'url') await page.goto(input, { waitUntil: 'networkidle0' });

	//To reflect CSS used for screens instead of print
	await page.emulateMediaType('screen');

	//

	const body = await page.$('body');
	const body_box = await body.boundingBox();
	var row = await page.$('.row-1 .row-content'),
	row_box;
	if (row) row_box = await row.boundingBox();
	else {
		row = body;
		row_box = body_box;
	}

	// Downlaod the PDF
	const pdf = await page.pdf({
		path: pdfPath,
		margin: { top: '0px', right: '0px', bottom: '0px', left: '0px' },
		printBackground: true,
		width: row_box.width + 300,
		height: body_box.height + 50
	});

	await browser.close();

	//////
	//////
	//////
	//////
	//////
	//////

	// pdf

	// headerHeight = 100;

	/* var fileData = htmlContent.match(/\<\!\-\-\s*PDF\#(\{\s*[\-\_\s\"\'\w\d\.\:]*\s*\})\#PDF\s*\-\-\>/);
	var data;
	try{
		data = JSON.parse(fileData[1]);
	}catch(err){
		data = {};
	}
	
	await pdf.create({
		html: htmlContent,
		data: {},
		path: pdfPath
	}, Object.assign({}, pdfOptions, data/ * , {
		header: {
			height: headerHeight + 'px',
			contents: `<table style="width: 100%; height: 100%; border-bottom: solid 1px;" border="0" cellspacing="0" cellpadding="0"><tr><td valign="center" style="text-align: center; padding-top: 0.24em; font-size: 4rem; line-height: 1;">${fileName}</td></tr></table>`
		}
	} * /)); */

	//////
	//////
	//////
	//////
	//////
	//////

	log('{{success}}' + pdfPath);
}

(async function() {
	var i, j, input, currentPath;
	for(i = 0; i < process.arguments.input.length; i++){
		input = process.arguments.input[i];
		if (input.substr(0, 4) != 'http') {
			currentPath = path.resolve(input);
			if (input == '.') {
				items = fs.readdirSync(input, {withFileTypes: true})
				.filter(item => !item.isDirectory() && path.extname(item.name) == '.html' && item.name.charAt(0) != '.')
				.map(item => item.name);
				for (j = 0; j < items.length; j++) {
					await buildPdf(currentPath + '/' + items[j]);
				}
			}
			else await buildPdf(currentPath);
		}
		else await buildPdf(input);
	}
}());